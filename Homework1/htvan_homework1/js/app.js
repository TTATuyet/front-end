window.addEventListener("scroll", show_menu);
    function show_menu() {
        if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
            document.getElementById("fixed_top_menu").classList.remove("d-none");
        } else {
            document.getElementById("fixed_top_menu").classList.add("d-none");
        }
    }

$(document).ready(function(){

    $('.items').slick({
    dots: true,
    infinite: true,
    speed: 800,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
    {
    breakpoint: 1024,
    settings: {
    slidesToShow: 3,
    slidesToScroll: 3,
    infinite: true,
    dots: true
    }
    },
    {
    breakpoint: 600,
    settings: {
    slidesToShow: 2,
    slidesToScroll: 2
    }
    },
    {
    breakpoint: 480,
    settings: {
    slidesToShow: 1,
    slidesToScroll: 1
    }
    }
    
    ]
    });
});

function hambugerClickFunction() {
    var x = document.getElementById("fixed_search_nav");
    var y = document.getElementById("fixed_search_nav_mobile");
    var intFrameWidth = window.innerWidth;
    if (intFrameWidth > 768) {
        y.classList.add("d-none");
        if (x.classList.contains('d-none')) {
            x.classList.remove("d-none");
        } else {
            x.classList.add("d-none");
        }
    } else {
        x.classList.add("d-none");
        if (y.classList.contains('d-none')) {
            y.classList.remove("d-none");
        } else {
            y.classList.add("d-none");
        }
    }
}