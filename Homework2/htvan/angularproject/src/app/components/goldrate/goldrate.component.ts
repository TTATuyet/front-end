import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import { GoogleChartInterface } from 'ng2-google-charts';
import { interval, Observable } from 'rxjs';
import { GoldData } from 'src/app/models/gold-data';
import { GoldDataServiceService } from 'src/app/services/gold-data-service.service';


@Component({
  selector: 'app-goldrate',
  templateUrl: './goldrate.component.html',
  styleUrls: ['./goldrate.component.css'],
})
export class GoldrateComponent implements OnInit {
  @ViewChild('chart',{static: false}) chart;
  goldData: GoldData[] = [];
  currentBuying = 0.0;
  currentSelling = 0.0;
  currentAverage = 0.0;
  arrowType: String;
  lineChart: GoogleChartInterface = {
    chartType: 'LineChart'
  }
  showSpinner = true;
  
  
  constructor(private goldDataService: GoldDataServiceService) {

   }

  ngOnInit(): void {

   // this.goldData = [
      // {"symbol":"gold","buy":52.022164511900016,"sell":56.977835488099984,"timestamp": new Date("2021-03-09T02:45:25.424+00:00")},
      // {"symbol":"gold","buy":56.96770103646805,"sell":52.03229896353195,"timestamp": new Date("2021-03-09T02:45:45.551+00:00")},
      // {"symbol":"gold","buy":51.89387559509773,"sell":57.10612440490227,"timestamp": new Date("2021-03-09T03:13:06.374+00:00")},
      // {"symbol":"gold","buy":54.5,"sell":54.5,"timestamp": new Date("2021-03-09T03:13:20.995+00:00")},
      // {"symbol":"gold","buy":52.04262675265699,"sell":56.95737324734301,"timestamp":new Date("2021-03-09T03:14:05.942+00:00")}
  //  ]
    //this.updateChart();


    interval(1000).subscribe(x => {
      this.showSpinner = false;
      this.getGoldData();
    });  
  }

  getGoldData() {
  
    this.goldDataService.getGoldData().subscribe((result) => {
      let val: any = result;
        let gold: GoldData = {
        symbol: val.symbol,
        buy: +val.buy,
        sell: +val.sell,
        timestamp: new Date(val.timestamp)
      }

      console.log(gold);
      
      this.goldData.push(gold);
      this.updateChart();

      this.currentBuying = +(gold.buy).toFixed(2);
      this.currentSelling = +(gold.sell).toFixed(2);

      let newAverage = (this.currentBuying+this.currentSelling)/2;
      if (newAverage >= this.currentAverage) {
        this.arrowType = "fa-arrow-up";
      } else {
        this.arrowType = "fa-arrow-down";
      }

      this.currentAverage = newAverage;

     });
  }


  updateChart() {
    
    let dataTable = [];
    dataTable.push(['Date','Buy','Sell']);
    this.goldData.forEach(data => {
      dataTable.push([data.timestamp,data.buy,data.sell]);
    })
    this.lineChart = {
      chartType: 'LineChart',
      dataTable: dataTable,
      options: {
        height: 600
      },
    };
    this.chart.draw();
  }
}
