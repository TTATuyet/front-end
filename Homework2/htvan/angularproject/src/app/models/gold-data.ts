export interface GoldData {
    symbol: string,
    buy: number,
    sell: number, 
    timestamp: Date
}