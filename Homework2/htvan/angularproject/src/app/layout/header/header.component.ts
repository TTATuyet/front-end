import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  

  hambugerClickFunction() {
    var x = document.getElementById("fixed_search_nav");
    var y = document.getElementById("fixed_search_nav_mobile");
    var intFrameWidth = window.innerWidth;
    if (intFrameWidth > 768) {
        y.classList.add("d-none");
        if (x.classList.contains('d-none')) {
            x.classList.remove("d-none");
        } else {
            x.classList.add("d-none");
        }
    } else {
        x.classList.add("d-none");
        if (y.classList.contains('d-none')) {
            y.classList.remove("d-none");
        } else {
            y.classList.add("d-none");
        }
    }
}

}
