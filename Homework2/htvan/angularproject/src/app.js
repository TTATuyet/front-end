window.addEventListener("scroll", show_menu);
    function show_menu() {
        if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
            document.getElementById("fixed_top_menu").classList.remove("d-none");
        } else {
            document.getElementById("fixed_top_menu").classList.add("d-none");
        }
    }
