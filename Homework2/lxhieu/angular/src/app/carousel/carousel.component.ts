import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Product } from '../product.model';
@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {
  datas:Product[]=[];

  slide1: Product[] = [];
  slide2: Product[] = [];


  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.getAll();

  }

  getAll(){
    this.productService.getAll().subscribe((res:any)=>{
      this.datas = res
      this.slide1 = this.datas.slice(0,4);
      this.slide2 = this.datas.slice(4,8);
    })
  }


}
