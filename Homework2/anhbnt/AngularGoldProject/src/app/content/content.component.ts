import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  lstCarousel = [{img: '', name: '', code: '', contact: ''}]
  constructor() { }

  ngOnInit(): void {
    this.lstCarousel = [
      {img:'https://baotinmanhhai.vn/web/uploads/lac_web_2.jpg', name:'Vòng tay mặt sao biển đá Emerald MH2-V', code:'Mã sản phẩm: MH2-V', contact:'Liên hệ'},
      {img:'https://baotinmanhhai.vn/web/uploads/product/Originals/bac/bac tre em/lac%20bi%20bac%20meo%20kitty%20xanh.jpg', name:'Lắc bi bạc trẻ em mèo Kitty xanh lá MH2 - L180426013', code:'Mã sản phẩm: MH2 - L180426013', contact:'Liên hệ'},
      {img:'https://baotinmanhhai.vn/web/uploads/product/Originals/bac/bac tre em/vong%20bac%20meo%20kitty.jpg', name:'Vòng bạc mèo Kitty cho bé MH2 - V180402001', code:'MH2 - V180402001', contact:'Liên hệ'},
      {img:'https://baotinmanhhai.vn/web/uploads/product/Originals/vang tay 2018/lắc/day3-169.jpg', name:'Vòng tay gắn đá Citrine MH1 - V', code:'Mã sản phẩm: MH1 - V', contact:'Liên hệ'},
      {img:'https://baotinmanhhai.vn/web/uploads/product/Originals/bac/bac xi - focus on me/IMG_4803.2.jpg', name:'Nhẫn bạc gắn đá trắng tròn N170825011', code:'Mã sản phẩm: N170825011', contact:'Liên hệ'},
      {img:'https://baotinmanhhai.vn/web/uploads/product/Originals/Bo Suu Tap/BST Family Love/nhan%20s%201.png', name:'Nhẫn mặt trái tim gắn đá Sapphire MH2 - N171212002', code:'Mã sản phẩm: MH2 - N171212002', contact:'Liên hệ'},
      {img:'https://baotinmanhhai.vn/web/uploads/product/Originals/vang tay 2018/nhan/nu/ruby/nhan-vang-nu-ruby-hoa-ket-kim.jpg', name:'Nhẫn vàng ruby nữ kết kim hình hoa  MH2 - N171006054', code:'Mã sản phẩm: MH2 - N171006054', contact:'Liên hệ'},
    ]
  }

}
