import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
imageCarousel1= [
  {
    img: '',
    name:'',
    code:'',
    contact:'',
  }

]

imageCarousel2= [
  {
    img: '',
    name:'',
    code:'',
    contact:'',
  }

]

  constructor() { }

  ngOnInit(): void {
    this.imageCarousel1 = [
      {
        img: 'https://baotinmanhhai.vn/web/uploads/lac_web_2.jpg',
        name:'Vòng tay mặt sao biển đá Emerald MH2-V',
        code:'Mã sản phẩm: MH2-V',
        contact:'Liên hệ',
      },
      {
        img: 'https://baotinmanhhai.vn/web/uploads/product/Originals/bac/bac%20tre%20em/lac%20bi%20bac%20meo%20kitty%20xanh.jpg',
        name:'Vòng tay mặt sao biển đá Emerald MH2-V',
        code:'Mã sản phẩm: MH2-V',
        contact:'Liên hệ',
      },
      {
        img: 'https://baotinmanhhai.vn/web/uploads/product/Originals/bac/bac%20tre%20em/vong%20bac%20meo%20kitty.jpg',
        name:'Vòng tay mặt sao biển đá Emerald MH2-V',
        code:'Mã sản phẩm: MH2-V',
        contact:'Liên hệ',
      },
      {
        img: 'https://baotinmanhhai.vn/web/uploads/product/Originals/vang%20tay%202018/l%E1%BA%AFc/day3-169.jpg',
        name:'Vòng tay mặt sao biển đá Emerald MH2-V',
        code:'Mã sản phẩm: MH2-V',
        contact:'Liên hệ',
      },

    ]


  }

}
